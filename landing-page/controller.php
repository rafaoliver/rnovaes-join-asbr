<?php
	///////////////////////////////////////////////////
	//
	// Developer: Raphael Novaes
	// Mail: rafaoliver@uol.com.br
	//
	///////////////////////////////////////////////////

	require_once('model.php');

	extract($_POST);

	$score = 10;
	$data_fix = '01/06/2016';
	$token = '77bc5905fccc100147848e4c5aa02cf7';

	$unidade = utf8_decode($unidade);

	//Score por regiao
	switch ($regiao) {
		case 'Sul':
			$score -= 2;
		break;
		case 'Sudeste':
			if($unidade != utf8_decode('São Paulo'))
				$score -= 1;
		break;
		case 'Centro-Oeste':
			$score -= 3;
		break;
		case 'Nordeste':
			$score -= 4;
		break;
		case 'Norte':
			$score -= 5;
			$unidade = 'INDISPONÍVEL';
		break;
	}

	$data_nascimento = implode('/', array_reverse(explode('/', $data_nascimento)));
	$data_nascimento = strtotime($data_nascimento);
	
	$data_fix = implode('/', array_reverse(explode('/', $data_fix)));
    $data_fix = strtotime($data_fix);

    //Score por idade
    if($data_nascimento > $data_fix)
    	$idade = 0;
    else
    	$idade = floor(abs($data_fix-$data_nascimento)/60/60/24/365);

    if($idade > 99 || $idade < 18)
    	$score -= 5;

    if($idade > 39 && $idade < 100)
    	$score -= 3;

    $data_nascimento = date('Y-m-d', $data_nascimento);

    //Grava no BD
    inserir_lead();

    //Envia POST
    $url = 'http://api.actualsales.com.br/join-asbr/ti/lead';

    foreach($_POST as $key => $value) { $fields[$key] = urlencode(utf8_encode($$key)); }

    $fields['score'] = urlencode($score);
    $fields['token'] = urlencode($token);

	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
	rtrim($fields_string, '&');

	$ch = curl_init();

	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	$result = curl_exec($ch);

	curl_close($ch);
?>