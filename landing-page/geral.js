///////////////////////////////////////////////////
//
// Developer: Raphael Novaes
// Mail: rafaoliver@uol.com.br
//
///////////////////////////////////////////////////

function validform(){
	var ret = true;
	$('.form-control:visible').map(function(){
		regex = rgx($(this).attr('name'));
		if(regex.test($(this).val()) == false){
			ret = false;
			$(this).addClass('invalid');
		}else{
			$(this).removeClass('invalid');
		}
	});
	return ret;
}

function rgx(nme){
	switch (nme) {
		case 'nome':
			var regex = new RegExp('^[a-zA-Z\u00C0-\u00FF]+[ ][a-zA-Z\u00C0-\u00FF ]+$'); 
		break;

		case 'data_nascimento':
			var regex = new RegExp('^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$');
		break;

		case 'email':
			var regex = new RegExp('^[A-z0-9._%+-]+@[A-z0-9.-]+.[A-z]{2,4}$');
		break;

		case 'telefone':
			var regex = new RegExp('^[(1-9)]{4}[2-9][0-9]{7,8}$');
		break;
		default:
			var regex = new RegExp('^[a-zA-Z\u00C0-\u00FF -]+$');
		break;
	}

	return regex;
}

function changeRegion(elem){
	var units = {
		'Sul' : ['Porto Alegre', 'Curitiba'],
		'Sudeste' : ['São Paulo', 'Rio de Janeiro', 'Belo Horizonte'],
		'Centro-Oeste' : ['Brasília'],
		'Nordeste' : ['Salvador', 'Recife'],
		'Norte' : ['Não possui disponibilidade'],
	};

	optfirst = $('.unit option:first').clone();
	$('.unit').empty().append(optfirst);

	units[$(elem).val()].map(function(val){
		$('<option>',{text: val, value: val}).appendTo('.unit');
	});
}

function ajaxSend(){
	$.post( 
		"controller.php",
		$( "#step_1, #step_2" ).serialize(),
		function(data){
			console.log(data);
		},
		'json'
	);	
}

$(
	function () {
    	$('.next-step').click(function (event) {
    		if(validform() == true){
    			if($(this).hasClass('send')) ajaxSend();
	        	event.preventDefault();
	        	$(this).parents('.form-step').hide().next().show();
	        }else{
	        	return false;
	        }
    	});
	
		$('.form-control:visible').blur(function(){
			regex = rgx($(this).attr('name'));

			if(regex.test($(this).val()) == false){
				$(this).addClass('invalid').nextUntil().show();
			}else{
				$(this).removeClass('invalid').nextUntil().hide();
			}
		});
	}
);
