<?php
	///////////////////////////////////////////////////
	//
	// Developer: Raphael Novaes
	// Mail: rafaoliver@uol.com.br
	//
	// TABELA
	//
	// create table lead (
	//	 id_lead int(10) not null auto_increment, 
	//	 nome varchar(100) not null, 
	//	 data_nascimento date not null, 
	//	 email varchar(100) not null, 
	//	 telefone varchar(15) not null, 
	//	 regiao varchar(100) not null, 
	//	 unidade varchar(100) not null,
	//	 score int(10) not null,
	//	 primary key(id_lead)
	// );
	//
	///////////////////////////////////////////////////

	$link = mysqli_connect("localhost", "root", "", "actualsale");

	function inserir_lead(){
		global $link, $nome, $data_nascimento, $email, $telefone, $regiao, $unidade, $score;

		$prep = $link->prepare("INSERT INTO lead VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)");

		$prep->bind_param('ssssssi',$nome, $data_nascimento, $email, $telefone, $regiao, $unidade, $score);
		$prep->execute();
		$prep->close();
	}
?>